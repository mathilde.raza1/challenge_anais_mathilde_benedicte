terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}

provider "virtualbox" {
  # Configuration options for the VirtualBox provider
}

resource "virtualbox_vm" "ubuntuvm1" {
  name     = "ubuntu-vm-1"
  image    = "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.ova"
  cpus     = 2
  memory   = "2048 mib"
  ip       = 192.168.56.201
  network_adapter {
    type           = "nat"
    host_interface = "eth0"
  }
}

resource "virtualbox_vm" "ubuntuvm2" {
  name     = "ubuntu-vm-2"
  image    = "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.ova"
  cpus     = 2
  memory   = "2048 mib"
  ip       = 192.168.56.202
  network_adapter {
    type           = "nat"
    host_interface = "eth0" # Replace with your host interface name
  }
}

resource "virtualbox_vm" "haproxy" {
  name     = "haproxy"
  image    = "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.ova"
  cpus     = 1
  memory   = "1024 mib"
  ip       = 192.168.56.203
  network_adapter {
    type           = "nat"
    host_interface = "eth0" # Replace with your host interface name
  }
}