# Challenge Système Réseaux S8 - Application - Ansible - Terraform


Anais SALEZ 4A IT 
Bénédicte ROCHE 4A IT
Mathilde RAZAFIMAHATRATRA 4A IT


## Objectifs

Au cours ce challenge plusieurs objectifs sont à réaliser : 
- [Création d'une application web](https://gitlab.com/mathilde.raza1/challenge_anais_mathilde_benedicte/-/blob/main/ServerFlask/app.py)
- [On conteneurise l'application web avec Docker](https://gitlab.com/mathilde.raza1/challenge_anais_mathilde_benedicte/-/blob/main/ServerFlask/Dockerfile)
- [Création d'un playbook Ansible permettant le déploiement de l'application](https://gitlab.com/mathilde.raza1/challenge_anais_mathilde_benedicte/-/blob/main/Ansible/playbook.yml)
- [Utilisation de Terraform pour provisionner et gérer l'infrastructure ncéessaire pour le déploiement](https://gitlab.com/mathilde.raza1/challenge_anais_mathilde_benedicte/-/blob/main/Terraform/main.tf)

Les informations concernant la réalisation du projet son retrouvable sur le compte-rendu.pdf